import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_required_field/InputField.dart';
import 'package:rxdart/rxdart.dart';




class InputFieldList extends StatefulWidget {

  final List<InputField> fields;
  final Function(BuildContext, InputField) itemBuilder;
  final Function(BuildContext, int) separatorBuilder;
  final Function(bool isFilled) onFill;

  final BehaviorSubject<bool> fillController;
  final bool autovalidate;

  const InputFieldList({Key key,
    @required this.itemBuilder,
    this.fillController,
    this.separatorBuilder,
    this.onFill,
    this.fields,
    this.autovalidate = true,
  }) : super(key: key);

  @override
  InputFieldListState createState() => InputFieldListState();
}

class InputFieldListState extends State<InputFieldList> {

  final _formKey = GlobalKey<FormState>();
  Map<int, bool> _currentState;
  bool _isAcceptable = false;

  @override
  void initState() {
    super.initState();
    _currentState = {};

    // Initiate state storage with required field's keys
    _currentState.addEntries(
      (widget.fields ?? [])
        .where((field) => field.required)
        .map((field) => MapEntry(field.hashCode, false))
    );

       (widget.fields ?? [])
      .where((field) => field.required)
      .forEach((field) {
        _updateFillStatus(field);
        field.controller.addListener(() {
          _updateFillStatus(field);          
        });
      });
  }

  _updateFillStatus(InputField field) {
    // Put current filling state of controller to state storage
    _currentState[field.hashCode] = field.controller.text.isNotEmpty;

    // Check if all required fields are filled
    final currentAcceptable = _currentState.values.every((e) => e);

    if (_isAcceptable != currentAcceptable) {
      _isAcceptable = currentAcceptable;

      if (widget.fillController != null)
        widget.fillController.sink.add(_isAcceptable);
    }
  }

  @override
  void dispose() {
    if (widget.fillController != null)
      widget.fillController.close();
    super.dispose();
  }

  bool validate() => _formKey.currentState.validate();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      autovalidate: widget.autovalidate,
      child: ListView.separated(
        shrinkWrap: true,
        primary: false,
        itemCount: widget.fields.length,
        itemBuilder: (context, index) => widget.itemBuilder(context, widget.fields[index]),
        separatorBuilder: widget.separatorBuilder ?? (_, __) { return Container(); },
      ),
    );
  }
}