import 'package:flutter/material.dart';

enum InputFieldAction {
  Date, Time, DateTime
}

// ignore: must_be_immutable
class InputField extends StatelessWidget {
  final bool required;

  final String hint;
  final TextEditingController controller;
  final bool readOnly;
  final bool obscureText;
  final Function(String) validator;
  final int maxLength;
  TextStyle style;
  InputDecoration decoration;
  TextInputType keyboardType;
  InputFieldAction action;

  InputField({
    Key key,
    this.controller,
    this.hint,
    this.readOnly=false,
    this.obscureText=false,
    this.required=false,
    this.maxLength,
    this.decoration,
    this.keyboardType,
    this.action,
    this.validator,
    this.style
  }) : super();

  @override
  Widget build(BuildContext context) {
    if (decoration == null)
      decoration = InputDecoration(
        hintText: hint
      );

    return TextFormField(
      controller: controller,
      decoration: decoration,
      keyboardType: keyboardType,
      readOnly: readOnly,
      validator: validator,
      obscureText: obscureText,
      maxLength: maxLength,
      style: style,
      onTap: () {
        if (action == InputFieldAction.Date) {
          var initialDate;
          try {
            initialDate = DateTime.parse(controller.text);
          } catch(e) {
            initialDate = DateTime.now();
          }
          showDatePicker(
            context: context,
            initialDate: initialDate,
            firstDate: DateTime(1940),
            lastDate: DateTime.now()
          ).then((value) {
            controller.text = value.toIso8601String().split("T")[0];
          });
        }
      },
    );
  }
}
